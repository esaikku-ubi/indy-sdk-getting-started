## indy-sdk Getting Started tutorial

Copied from [indy-sdk getting started docs](https://github.com/hyperledger/indy-sdk/tree/master/docs/getting-started).

Docker environment for running indy-sdk getting started tutorial. Original didn't work anymore with ubuntu 16.04 due to changed packages so I updated to 18.04 and adjusted packages to be installed.

Check out the other md files for information about the getting started tutorial. There is also a sequence diagram done with plantuml that shows all the requests and how they happen between different agents and the ledger.

## Requirements

You need to have Docker installed.

## Running

Start the environment with this command that will build the images if they don't exist yet.

```
docker-compose up
```

Then copy the URL starting with `http://127.0.0.1:8888/?token=` to your browser to connect to the jupyter notebook server. 

Open the getting-started notebook, click on **Trust** button in upper righ corner and start running the cells from the beginning. The first cell has all the helper functions so it needs to be executed first. Most of the cells depend on actions in the previous cells so it's best run the cells in order.